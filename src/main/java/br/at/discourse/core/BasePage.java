package br.at.discourse.core;

import java.util.ArrayList;
import java.util.List;
import static br.at.discourse.core.DriverFactory.getDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

public class BasePage {
	
	/********* Alternar P�ginas ************/	
	public void alterPage(int indice) { 
	List<String> abas = new ArrayList<String>(getDriver().getWindowHandles());
	getDriver().switchTo().window(abas.get(indice));
	}
	
	/********* Link ************/
	
	public void clicarLink(String link) {
		getDriver().findElement(By.linkText(link)).click();
	}


	
	/************** JS *********************/
	
	public Object executarJS(String cmd, Object... param) {
		JavascriptExecutor js = (JavascriptExecutor) getDriver();
		return js.executeScript(cmd, param);
	}
	
	/************** Tabela *********************/
	
	public WebElement obterCelula(String colunaBusca, String colunaTopico){
		//procurar coluna do registro
		WebElement tabela = getDriver().findElement(By.xpath("//table"));
		int idColuna = obterIndiceColuna(colunaBusca, tabela);
		
		//encontrar a linha do registro
		int idLinha = obterIndiceLinha(tabela, idColuna);
		
		//procurar coluna do T�pico
		int idColunaTopico = obterIndiceColuna(colunaTopico, tabela);
		
		//clicar no botao da celula encontrada
		WebElement celula = tabela.findElement(By.xpath(".//tr["+idLinha+"]/td["+idColunaTopico+"]/span"));
		return celula;
	}
	
	public String obterTituloTopico(String colunaBusca, String colunaTopico){
		WebElement celula = obterCelula(colunaBusca, colunaTopico);
		return celula.getText();
		
	}

	protected int obterIndiceLinha(WebElement tabela, int idColuna) {
		List<WebElement> linhas = tabela.findElements(By.xpath(".//td["+idColuna+"]/span"));
		int idLinha = -1;
		double max = 0;
		for(int i = 0; i < linhas.size(); i++) {
			if(Double.parseDouble(linhas.get(i).getText().replace("k", "00")) > max) {
				max = Double.parseDouble(linhas.get(i).getText());
				idLinha = i;
			}
		}
		return idLinha;
	}

	protected int obterIndiceColuna(String coluna, WebElement tabela) {
		List<WebElement> colunas = tabela.findElements(By.xpath(".//th"));
		int idColuna = -1;
		for(int i = 0; i < colunas.size(); i++) {
			if(colunas.get(i).getText().equals(coluna)) {
				idColuna = i+1;
				break;
			}
		}
		return idColuna;
	}
}
