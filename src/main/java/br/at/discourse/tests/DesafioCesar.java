package br.at.discourse.tests;
import static br.at.discourse.core.DriverFactory.getDriver;
import java.util.Collections;
import java.util.List;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import br.at.discourse.pages.DemoPage;
import br.at.discourse.pages.HomePage;
import br.at.discourse.core.BasePage;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DesafioCesar {
	
	HomePage homePage = new HomePage();
	DemoPage demoPage = new DemoPage();
	BasePage basePage = new BasePage();
	int closedTopic = 0;
	
	//ABRE P�GINA INICIAL E VALIDA
	@Test
	public void T1_validaPaginaInicial() throws InterruptedException {
		homePage.acessarPaginaInicial();
		Assert.assertEquals("Discourse - Civilized Discussion", getDriver().getTitle());		
		
	}
	
	//ACESSA P�GINA DEMO E VALIDA
	@Test
	public void T2_validaPaginaDemo() throws InterruptedException {		
		homePage.clicarDemo();
		homePage.mudarAbaDemo(1);
		Assert.assertEquals("Demo", getDriver().getTitle());
		
	}	
	
	//EXIBE TODOS OS T�PICOS FECHADOS
	@Test
	public void T3_exibeTopicosFechados() throws InterruptedException {
		demoPage.rolarScroll();
		List<WebElement> linhas = getDriver().findElements(By.xpath("//tbody/tr/td/span[div/span]/a"));
		for(int i = 0; i < linhas.size(); i++) {
			if(!linhas.get(i).getText().contains("Welcome to our demo!"))
				System.out.println("T�pico Fechado: "+linhas.get(i).getText());
		}		
	}
	
	//EXIBE A QUANTIDADE DE T�PICOS POR CATEGORIA
	@Test
	public void T4_qtdItensCategoria() throws InterruptedException {
		List<WebElement> linhasGeral = getDriver().findElements(By.xpath("//tbody/tr/td/span/a"));
		List<WebElement> linhasComCategoria = getDriver().findElements(By.xpath("//*/td/div/a/span/span"));

		for(int i = 0; i < demoPage.capturaCategorias().size(); i++) {
		System.out.println(demoPage.capturaCategorias().get(i)+": "+Collections.frequency(demoPage.capturaTopicos(), demoPage.capturaCategorias().get(i)));
		}
		System.out.println("Sem Categoria: " + (linhasGeral.size() - linhasComCategoria.size()));
	}
	
	//EXIBE O T�PICO COM MAIS VISUALIZA��ES
	@Test
	public void T5_exibeTopView() throws InterruptedException {
		System.out.println("T�pico com mais views: " + basePage.obterTituloTopico("Views","Topic"));		
	}
	
}
