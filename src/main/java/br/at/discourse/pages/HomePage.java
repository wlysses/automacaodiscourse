package br.at.discourse.pages;

import static br.at.discourse.core.DriverFactory.getDriver;

import org.openqa.selenium.support.ui.WebDriverWait;

import br.at.discourse.core.*;

public class HomePage extends BasePage {
	
	WebDriverWait wait = new WebDriverWait(getDriver(), 50);
	
	public void acessarPaginaInicial(){
		DriverFactory.getDriver().get("https://www.discourse.org");
	}	
	public void clicarDemo(){
		clicarLink("Demo");		
	}	
	public void mudarAbaDemo(int indice){
		alterPage(indice);		
	}

}
