package br.at.discourse.pages;

import static br.at.discourse.core.DriverFactory.getDriver;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import br.at.discourse.core.*;

public class DemoPage extends BasePage {
	
	WebDriverWait wait = new WebDriverWait(getDriver(), 50);
	
	public void rolarScroll() throws InterruptedException{		
		WebElement footer = getDriver().findElement(By.tagName("footer"));
		executarJS("window.scrollTo(0, document.body.scrollHeight)", footer);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.tagName("h3")));
		executarJS("window.scrollTo(0, document.body.scrollHeight)", footer);
	}
	
	//CAPTURA TODAS AS CATEGORIAS 
	public List<String> capturaCategorias() {		
			List<WebElement> linhasComCategoria = getDriver().findElements(By.xpath("//*/td/div/a/span/span"));
			List<String> listaGeralCategorias = new ArrayList<String>();
			for(int i = 0; i < linhasComCategoria.size(); i++) {
				listaGeralCategorias.add(linhasComCategoria.get(i).getText());
			}
			List<String> categorias = Lists.newArrayList(Sets.newHashSet(listaGeralCategorias));
			return categorias;
		}
		
	//CAPTURA TODOS OS T�PICOS COM CATEGORIA
	public List<String> capturaTopicos() {		
			List<WebElement> linhasComCategoria = getDriver().findElements(By.xpath("//*/td/div/a/span/span"));
			List<String> listaGeralCategorias = new ArrayList<String>();
			for(int i = 0; i < linhasComCategoria.size(); i++) {
				listaGeralCategorias.add(linhasComCategoria.get(i).getText());
			}	
			return listaGeralCategorias;
		}
	
	

}
