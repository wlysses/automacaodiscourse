package br.at.discourse.suites;
import static br.at.discourse.core.DriverFactory.killDriver;

import java.io.IOException;

import org.junit.After;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import br.at.discourse.tests.DesafioCesar;
import br.at.discourse.core.Propriedades;


@RunWith(Suite.class)
@SuiteClasses({
	DesafioCesar.class,
})

public class SuiteGeral {
	@After
	public void finaliza() throws IOException{	
		if(Propriedades.FECHAR_BROWSER) {
			killDriver();
		}
	}
}

